const restify = require('restify');
const project = require('./package.json');
const pengaduanController = require('./controllers/pengaduan.controller');


function AppServer() {
  // create server
  this.server = restify.createServer({
    name: `${project.name}-server`,
    version: project.version
  });
  
  this.server.use(restify.plugins.bodyParser());
  this.server.use(restify.plugins.queryParser());

  // root
  this.server.get('/', (req, res) => {
    res.send({ success:true, data:'index', message:'This service is running properly', code:200 });
  });

  // book route
  this.server.get('/me', pengaduanController.getHandler);
  this.server.get('/me/user', pengaduanController.getbynameHandler);
  this.server.post('/pengaduan', pengaduanController.postHandler);
  this.server.put('/pengaduan/:id', pengaduanController.putHandler);
  this.server.del('/pengaduan/:id', pengaduanControlleri.deleteHandler);
}

module.exports = AppServer;